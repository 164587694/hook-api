使用hook技术检测app内方法调用

1. 安装 xposed/VirtualXposed_0.18.2.apk
    项目文档：https://github.com/android-hacker/VirtualXposed/blob/vxp/CHINESE.md
2. 打开VirtualXposed，长按VirtualXposed桌面，底部可安装应用
3. 安装HookApi
4. 在Xposed Install（自带app）中勾选HookApi模块
5. 运行需要检测的app
6. 可以使用adb logcat查看HookApi打印的日志